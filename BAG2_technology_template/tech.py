from . import BAG2_technology_definition


class TechInfo(BAG2_technology_definition):
    """ Compatibility class for BAG_framework.
    All technology definitions are transferred to 
    BAG2_technology_definition

    """

    def __init__(self, process_params):
        super().__init__(process_params)
