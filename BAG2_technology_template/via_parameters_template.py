"""
=========================
BAG2 Via parameter module 
=========================

Collection of Via related technology para,eters for 
BAG2 framework

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 11.04.2022.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""
import os
import pkg_resources
from typing import *
import yaml

# To use this file as starting point for via_parameters class comment the following lines
from abc import ABCMeta, abstractmethod


class via_parameters_template(metaclass=ABCMeta):

    # Then  uncomment the followint lines
    # from BAG2_technology_template.via_parameters_template import via_parameters_template
    # class via_parameters(via_parameters_template):

    # Then, delete the lines with '@abstractmethod' and start filling in the right values

    @property
    @abstractmethod
    def via_units(self) -> Dict:
        """Property that returns a dict of via units of instances of class 'via_unit'

        """
        if not hasattr(self, '_via_units'):
            self._via_units = {}
            name = 'CONT'
            # Next unit
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = {
                # Dimensions
                'dim': [int, int],
                # via horizontal/vertical spacing
                'sp': [int, int],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[int, int], [int, int]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [[int, int], [int, int]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {
                    # via enclosure rule as function of wire width.
                    'w_list': [int, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [[int, int]],
                        [[int, int]],
                    ]
                }
            }

            # Next unit
            name = 'VIA1'
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = {
                # Dimensions
                'dim': [int, int],
                # via horizontal/vertical spacing
                'sp': [int, int],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                # 'sp2': [[int , int ], [int , int ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                # 'sp3' : [[int , int ], [int , int ]]
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {
                    # via enclosure rule as function of wire width.
                    'w_list': [int, int, int, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                    ]
                }
            }
            self._via_units[name].hrect = {
                # Dimensions
                'dim': [int, int],
                # via horizontal/vertical spacing
                'sp': [int, int],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                # 'sp2': [[int , int ], [int , int ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                # 'sp3' : [[int , int ], [int , int ]]
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {
                    # via enclosure rule as function of wire width.
                    'w_list': [int, int, int, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [[int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int]],
                    ]
                }
            }
            # Next unit
            name = 'x1'
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = {
                # Dimensions
                'dim': [int, int],
                # via horizontal/vertical spacing
                'sp': [int, int],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                # 'sp2': [[int , int ], [int , int ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                # 'sp3' : [[int , int ], [int , int ]]
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {
                    # via enclosure rule as function of wire width.
                    'w_list': [int, int, int, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                    ]
                }
            }
            self._via_units[name].hrect = {
                # Dimensions
                'dim': [int, int],
                # via horizontal/vertical spacing
                'sp': [int, int],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                # 'sp2': [[int , int ], [int , int ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                # 'sp3' : [[int , int ], [int , int ]]
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {
                    # via enclosure rule as function of wire width.
                    'w_list': [int, int, int, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [[int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int], [int, int], [int, int]],
                    ]
                }
            }
            # Next unit
            name = 'x2'
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = {
                # Dimensions
                'dim': [int, int],
                # via horizontal/vertical spacing
                'sp': [int, int],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[int, int], [int, int], [int, int]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [[int, int]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [[int, int], [int, int], [int, int], [int, int], [int, int]],
                    ]
                }
            }
            # Next unit
            name = 'xint '
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = {
                # Dimensions
                'dim': [int, int],
                # via horizontal/vertical spacing
                'sp': [int, int],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[int, int], [int, int], [int, int]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [[int, int]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {
                    # via enclosure rule as function of wire width.
                    'w_list': [int, int, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [[int, int], [int, int], [int, int], [int, int]],
                        [[int, int], [int, int]],
                        [[int, int]],

                    ]
                }
            }
        return self._via_units

    @property
    @abstractmethod
    def via_name(self) -> Dict[int, str]:
        """ Dictionary of via types between layer types.

        """
        return {
            0: 'CONT',
            1: 'VIA1',
            2: 'x1',
            3: 'x1',
            4: 'x1',
            5: 'x1',
            6: 'x2',
            7: 'x3',
            8: 'x3',
            9: 'x3',
            10: 'x3',
        }

    @property
    @abstractmethod
    def via_id(self) -> Dict[Tuple[str, str], str]:
        """ Dictionary for mapping a via to connect of two layes to a 
        via primitive of the vendor.

        """
        return {
            (('layer', 'purpose'), 'layer'): 'name',
            (('layer', 'purpose'), 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
            ('layer', 'layer'): 'name',
        }

    @property
    @abstractmethod
    def property_dict(self) -> Dict:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        devel = False
        if not hasattr(self, '_property_dict'):
            if devel:
                yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
                with open(yaml_file, 'r') as content:
                    # self._process_config = {}
                    dictionary = yaml.load(content, Loader=yaml.FullLoader)

                self._property_dict = dictionary['via']
            else:
                self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self, key)

            for key, val in self.via_units.items():
                self._property_dict[key] = val.property_dict

        return self._property_dict


class via_unit:
    """Class for via instances

    """

    def __init__(self, **kwargs):
        if kwargs.get('name'):
            self._name = kwargs.get('name')
        else:
            print('Must give name for a via instance')

    @property
    @abstractmethod
    def name(self) -> Optional[str]:
        if not hasattr(self, '_name'):
            self._name = None
        return self._name

    @property
    @abstractmethod
    def square(self) -> Dict[str, Any]:
        """Definition dictionary of a square via.

        Keys:
        dim : [int,int]
                Dimensions
        sp  : [ int, int]
                Via horizontal/vertical spacing
        sp2: [[int, int], [int, int]]
                List of valid via horizontal/vertical spacings when it's a 2x2 array
                Default: Nonexistent
        sp3 : [[int, int], [int, int]],
                List of valid via horizontal/vertical spacings when it's a mxn array,
                where min(m, n) >= 2, max(m, n) >= 3
                Default: Nonexistent
        bot_enc : None,
                Via bottom enclosure rules.  If empty, then assume it's the same
                as top enclosure.
                # via top enclosure rules.
        top_enc : Dict
               Via enclosure rule as function of wire width.
               Keys:
                   w_list : [int, float('Inf')]
                            Width list
                   enc_list : [
                        [[6, 6]],
                        [[6, 6]],
                        ]
                            List of list of lists of valid horizontal/vertical enclosures.
                            Via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                            is used if wire width is less than or equal to w_list[idx].

        """

        if not hasattr(self, '_square'):
            self._square = None
        return self._square

    @square.setter
    def square(self, val: Dict[str, Any]):
        self._square = val

    @property
    @abstractmethod
    def hrect(self) -> Dict[str, Any]:
        """ Shape definition of a horizontal rectangle
        Vertical data is derived from this.

        Default: same as the square.
        """

        if not hasattr(self, '_hrect'):
            self._hrect = None
        return self._hrect

    @hrect.setter
    def hrect(self, val: Dict[str, Any]):
        self._hrect = val

    @property
    @abstractmethod
    def property_dict(self) -> Dict:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        if not hasattr(self, '_property_dict'):
            self._property_dict = {}
            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict' and key != 'name':
                    try:
                        self._property_dict[key] = getattr(self, key)
                    except NotImplementedError:
                        pass  # skip unimplemented parameters (i.e. FINFET for a CMOS tech)
        return self._property_dict
