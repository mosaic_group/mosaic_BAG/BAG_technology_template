"""
=====================================
BAG2 layer parameters template module
=====================================

The layer definition module of BAG2 framework. Currently collects all definition not
directly related to 

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 2020.

The goal of the class is to provide a master source for the process information. 
Currently it inherits TechInfoConfig, which in turn uses the values defined in it.
This circularity should be eventually broken.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""
import os
import pkg_resources
from typing import *
import yaml

# To use this file as starting point for layers_parameters class comment the following lines
from abc import ABCMeta, abstractmethod


class layer_parameters_template(metaclass=ABCMeta):
    # Then  uncomment the followint lines
    # from BAG2_technology_template.layer_parameters_template import layer_parameters_template
    # class layer_parameters(mos_parameters_template):

    # Then, delete the lines with '@abstractmethod' and start filling in the right values

    @property
    @abstractmethod
    def tech_lib(self) -> str:
        """ Name of the process technology library containing transistor primitives.

        """
        return 'tech_lib_name'

    @property
    @abstractmethod
    def layout_unit(self) -> float:
        """Layout unit, in meters

        """
        return 1.0e-6

    @property
    @abstractmethod
    def resolution(self) -> float:
        """Layout resolution in layout units.
        
        """
        return 0.001

    @property
    @abstractmethod
    def flip_parity(self) -> bool:
        """True if this is multipatterning technology. 

        type: bool
        
        [TODO] Define where used and for what.
        There is a method 'use_flip_parity' for this name in 
        TechInfoConfig, so therefore we can not 
        define the property with a same name.
        
        """
        pass

    @property
    @abstractmethod
    def pin_purpose(self) -> str:
        """Purpose of the pin layer used in pins constructs
            
              str : Default 'pin'

        """
        pass

    @property
    @abstractmethod
    def well_layers(self) -> Dict[str, List[Tuple[str, str]]]:
        """Dictionary of tuples of form 
        key:  (layer,purpose)
        describing the layers used for ntap and ptap.

        Current keys: 
                ntap : (str,str) 
                ptap : (str,str)

        """
        return {
            'ntap': [('layer', 'purpose')],
            'ptap': []
        }

    @property
    @abstractmethod
    def mos_layer_table(self) -> Dict[str, Tuple[str, str]]:
        """Dictionary of tuples of form 
        key:  (layer,purpose)
        describing the layers used for ntap and ptap.

        Current keys:
                PO : (str,str), Poly layer 
                PO_dummy : (str,str), Dummy POly layer
                OD : (str,str), Diffusion layer
                OD_dummy: (str,str), Dummy diffusion layer

        """
        return {
            'PO': ('layer', 'purpose'),
            'PO_dummy': ('layer', 'purpose'),
            'OD': ('layer', 'purpose'),
            'OD_dummy': ('layer', 'purpose'),
        }

    @property
    @abstractmethod
    def res_layer_table(self) -> Dict[str, Tuple[str, str]]:
        """Dictionary of tuples of form 
        key:  (layer,purpose)
        describing the layers used for a metal resistors.

        Current keys:
                RPDMY : (str,str), a layer drawn exactly on top of metal resistor,
                        generally used for LVS recognition purposes.

                RPO : (str,str), the "resistive poly" layer that makes a poly more resistive.
 
        """
        return {
            'RPDMY': ('layer', 'purpose'),
            'RPO': ('layer', 'purpose'),
        }

    @property
    @abstractmethod
    def res_metal_layer_table(self) -> Dict[int, List[Tuple[str, str]]]:
        """ Mapping from metal layer ID to layer/purpose pair that defines
        a metal resistor.
        
        Array of tuples of form (layer,purpose)
        Index 0 is usually (None,None) as there ins no Metal0

        """
        return {
            1: [('layer', 'purpose')],
            2: [('layer', 'purpose')],
            3: [('layer', 'purpose')],
            4: [('layer', 'purpose')],
            5: [('layer', 'purpose')],
            6: [('layer', 'purpose')],
            7: [('layer', 'purpose')],
            8: [('layer', 'purpose')],
            9: [('layer', 'purpose')],
            10: [('layer', 'purpose')],
            11: [('layer', 'purpose')],
        }

    @property
    @abstractmethod
    def metal_exclude_table(self) -> Dict[int, Tuple[str, str]]:
        """Mapping from metal layer ID to layer/purpose pair that
        defines metal exclusion region.
        
        Dict of tuples of form int : (layer,purpose)

        """
        return {
            1: ('layer', 'purpose'),
            2: ('layer', 'purpose'),
            3: ('layer', 'purpose'),
            4: ('layer', 'purpose'),
            5: ('layer', 'purpose'),
            6: ('layer', 'purpose'),
            7: ('layer', 'purpose'),
            8: ('layer', 'purpose'),
            9: ('layer', 'purpose'),
            10: ('layer', 'purpose'),
        }

    @property
    @abstractmethod
    def layer_name(self) -> Dict[Union[int, str], str]:
        """
        Mapping dictionary from metal layer ID to layer name. Assume purpose is 'drawing'.

        Dict of form
        
                For metals int : 'layer'
                For vias   'Vint': 'layer
                For contact 'Cint' : 'layer'
        
        Example:

            return {
                0 : 'DIFFLAYER',
                'C0': 'CONTAC0',
                'C1': 'CCONTAC1',
                  1 : 'METAL1',
                'V1': 'VIA1',
                  2 : 'METAL22',
            }
        """
        return {
            0: 'DIFFLAYER',
            'C0': 'CONTAC0',
            'C1': 'CCONTAC1',
            1: 'METAL1',
            'V1': 'VIA1',
            2: 'METAL2',
        }

    @property
    @abstractmethod
    def layer_type(self) -> Dict[str, str]:
        """Mapping from metal layer name to metal layer type.  The layer type
         is used to figure out which EM rules to use. OD has no current density
         EM rules, but layer type is used for via mapping as well. Hence, it
         is also here.

        Dict of of form int : 'layer'
        
        """
        return {
            'DIFFLAYER': 'od',
            'METAL1': '1',
            'METAL2': 't1',
            'METAL3': 't1',
            'METAL4': 't1',
            'METAL5': 't1',
            'METAL6': 't1',
            'METAL7': 't2',
            'METAL8': 't3',
            'METAL9': 't4',
        }

    @property
    @abstractmethod
    def max_w(self) -> Dict[str, int]:
        """ Maximum wire width for given metal type
        
        """
        return {
            '1': int,
            't1': int,
            't2': int,
            't3': int,
            't4': int,
        }

    @property
    @abstractmethod
    def min_area(self) -> Dict[str, List[int]]:
        """ Minimum area for given metal type
        
        """
        return {
            '1': [int],
        }

    @property
    @abstractmethod
    def sp_le_min(self) -> Dict[str, Dict[str, List[Union[int, float]]]]:
        """
        Minimum line-end spacing rule. Space is measured parallel to wire direction

        ------
        Returns
        Dict Structure (NOTE: a,b,x,y,z ∈ int):
        {
            w_list: [a, b, inf]    # values in units
            sp_list: [x, y, z]   # values are in units
        }

        Illustration:
            widths(unit):   min_w       a         b        inf
            space(unit):            x         y       z

        NOTE: The min_w is implicit (not "part" of the w_list)

        Interpretation:
            - Space x:  valid for width interval [0, a[
            - Space y:  valid for width interval [a, b[
            - Space z:  valid for width interval [b, inf[
        """
        return {
            '1': {
                'w_list': [int, int, int, float('Inf')],
                'sp_list': [int, int, int, int],
            },
            't1': {
                'w_list': [int, int, int, float('Inf')],
                'sp_list': [int, int, int, int],
            },
            't2': {
                'w_list': [int, int, float('Inf')],
                'sp_list': [int, int, int],
            },
            't3': {
                'w_list': [int, int, float('Inf')],
                'sp_list': [int, int, int],
            },
            't4': {
                'w_list': [float('Inf')],
                'sp_list': [int],
            },
        }

    @property
    @abstractmethod
    def len_min(self) -> Dict[str, Dict[str, Any]]:
        """
        Minimum length/minimum area rules.

        ------
        Returns
        Dict Structure:
        {
            w_list:    [a,   b,   inf] # values in units
            w_al_list: [(a1, l1), (a2, l2), (a3, l3)]    # area values are in square-units, length in units

            md_list:    [x, y, inf]
            md_al_list: [(a1, l1), (a2, l2), (a3, l3)]   # area values are in square-units, length in units
        }

        The pairs (`w_list`/`w_al_list` and `md_list`/`md_al_list`) belong together.

        ---

        The w-list is used to determine the minimum length for a given width.
        The index-correlated lists are searched through in forward direction,
        until the first w_list entry is found where given width fits.

        Illustration w-list:
            w_list(unit):     min_w        a            b            inf
            w_al_list(unit):      (a1, l1)    (a2, l2)     (a3, l3)
        NOTE: The min_w is implicit (not "part" of the w_list)

        Interpretation:
            - (a1, l1): valid area/length for width interval [min_w, a[
            - (a2, l2): valid area/length for width interval [a, b[
            - (a3, l3): valid for width interval [b, inf[

        ---

        The md-list is used to determine the maximum dimension for a given width.
        The index-correlated lists are searched through in reverse direction,
        until the first max_dim list entry is found where given width/length fits (is larger).
        """
        return {
            '1': {
                'w_list': [float('Inf')],
                'w_al_list': [[int, int]],
                'md_list': [int, int, float('Inf')],
                'md_al_list': [[int, int], [int, int], [int, int]]
            },
            't1': {
                'w_list': [float('Inf')],
                'w_al_list': [[int, int]],
                'md_list': [int, float('Inf')],
                'md_al_list': [[int, int], [int, int]],
            },
            't2': {
                'w_list': [float('Inf')],
                'w_al_list': [[int, int]],
                'md_list': [float('Inf')],
                'md_al_list': [[int, int]],
            },
            't3': {
                'w_list': [float('Inf')],
                'w_al_list': [[int, int]],
                'md_list': [float('Inf')],
                'md_al_list': [[int, int]],
            },
            't4': {
                'w_list': [float('Inf')],
                'w_al_list': [[int, int]],
                'md_list': [float('Inf')],
                'md_al_list': [[int, int]],
            },
        }

    @property
    @abstractmethod
    def idc_em_scale(self) -> Dict[str, Any]:
        """Table of electromigration temperature scale factor
        """
        return {
            # scale factor for resistor
            # scale[idx] is used if temperature is less than or equal to temp[idx]
            'res': {
                'temp': [int, int, int, int, int, float('Inf')],
                'scale': [float, float, float, float, float, float],
                # scale factor for this metal layer type
            },
            't4': {
                'temp': [
                    int, int, int, int, int, int,
                    int, int, int, int, int, float('Inf')
                ],
                'scale': [float, float, float, float, float, float, float, float, float, float, float, float],
            },
            # default scale vector
            'default': {
                'temp': [
                    int, int, int, int, int, int,
                    int, int, float('Inf')
                ],
                'scale': [float, float, float, float, float, float, float, float, float],
            }
        }

    @property
    @abstractmethod
    def sp_min(self) -> Dict[str, Dict[str, List[Union[float, int]]]]:
        """ Minimum wire spacing rule.  Space is measured orthogonal to wire direction.
        Wire spacing as function of wire width.  sp_list[idx] is used if wire width is 
        less than or equal to w_list[idx].

        """
        return {
            '1': {
                'l_list': [int, int, int, int, int, float('Inf')],
                'w_list': [int, int, int, int, int, float('Inf')],
                'sp_list': [int, int, int, int, int, int],
            },
            't1': {
                'l_list': [int, int, int, int, int, int, float('Inf')],
                'w_list': [int, int, int, int, int, int, float('Inf')],
                'sp_list': [int, int, int, int, int, int, int],
            },
            't2': {
                'l_list': [int, int, float('Inf')],
                'w_list': [int, int, float('Inf')],
                'sp_list': [int, int, int],
            },
            't3': {
                'l_list': [int, float('Inf')],
                'w_list': [int, float('Inf')],
                'sp_list': [int, int],
            },
            't4': {
                'l_list': [float('Inf')],
                'w_list': [float('Inf')],
                'sp_list': [int],
            }
        }

    @property
    @abstractmethod
    def sp_sc_min(self) -> Dict[str, Dict[str, List[Union[float, int]]]]:
        """ Minimum wire spacing rule for same color wires (FINFET specific parameter)

        """
        return {
            '1': {
                'l_list': [int, int, int, int, int, float('Inf')],
                'w_list': [int, int, int, int, int, float('Inf')],
                'sp_list': [int, int, int, int, int, int],
            },
            't1': {
                'l_list': [int, int, int, int, int, int, float('Inf')],
                'w_list': [int, int, int, int, int, int, float('Inf')],
                'sp_list': [int, int, int, int, int, int, int],
            },
            't2': {
                'l_list': [int, int, float('Inf')],
                'w_list': [int, int, float('Inf')],
                'sp_list': [int, int, int],
            },
            't3': {
                'l_list': [int, float('Inf')],
                'w_list': [int, float('Inf')],
                'sp_list': [int, int],
            },
            't4': {
                'l_list': [float('Inf')],
                'w_list': [float('Inf')],
                'sp_list': [int],
            }
        }

    @property
    @abstractmethod
    def dnw_margins(self) -> Dict[str, int]:
        """ Deep Nwell margins

        """
        return {
            'normal': int,
            'adjacent': int,
            'compact': int,
            'dnw_dnw_sp': int,  # Deep N-well to Deep N-well with different potential space
            'dnw_nw_sp': int,  # Deep N-well to N-well with different potential space
            'dnw_pw_sp': int,
            'dnw_np_enc': int,  # Minimum Deep N-well enclosure of NP
        }

    @property
    @abstractmethod
    def implant_rules(self) -> Dict[str, Any]:
        """' Implant layer rules.

        """
        return {
            'imp_ext_on_act': int,  # Minimum implantation extension from active OD
            'imp_ext_on_strap': int,  # Minimum implantation extension from active OD in N-well
            'imp_min_w': int,  # Minimum implant space
            'imp_min_sp': {  # Minimum implant to implant space (function of implant spacing)'' :
                'imp_w': [int, float('Inf')],
                'imp_sp': [int, int],
            }
        }

    @property
    @abstractmethod
    def nw_rules(self) -> Dict[str, int]:
        """ N-well layer rules.

        """
        return {
            'nw_min_w': int,  # Minimum N-well width
            'nw_min_sp': int,  # Minimum space for int N-well with same potential
            'nw_core_sp': int,  # Minimum space for int N-well with diff. potential (core device)'' :
            'nw_io_core_sp': int,  # Minimum between I/O device N-well and core device N-well with diff. pot.
            'nw_io_sp': int,  # Minimum space for int N-well with diff. potential (I/O device)'' :
            'nw_enc_on_strap': int,
        }

    @property
    @abstractmethod
    def od_rules(self) -> Dict[str, Any]:
        """ OD layer rules

        """
        return {
            'od_min_w': int,  # Minimum width of OD
            'od_min_sp': {  # Minimum spacing of two OD's (function of width)
                'od_w': [int, float('Inf')],
                'od_sp': [int, int],
            }
        }

    @property
    @abstractmethod
    def property_dict(self) -> Dict[str, Any]:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        devel = False
        # These are for development and debugging
        if not hasattr(self, '_property_dict'):
            if devel:
                yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
                with open(yaml_file, 'r') as content:
                    self._process_config = {}
                    dictionary = yaml.load(content, Loader=yaml.FullLoader)

                self._property_dict = dictionary
            else:
                self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    try:
                        if key == 'flip_parity':
                            self._property_dict['use_flip_parity'] = getattr(self, key)
                        else:
                            self._property_dict[key] = getattr(self, key)
                    except NotImplementedError:
                        pass  # skip unimplemented parameters (i.e. FINFET for a CMOS tech)
        return self._property_dict
