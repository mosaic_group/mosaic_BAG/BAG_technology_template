# BAG Technology Template

To be cloned to BAG2 virtuoso working dir under name 'BAG2_technology_definition'

Provides a class 'BAG2_technology_definition' defined in './BAG2_technology_definition/__init__.py'

Currently has just templates for the needed files. 

## Files you need to modify

BAG_prim - Virtuoso schematic templates for transistors

BAG_technology_definition/tech_params.yaml 
Process description for almost everything

BAG_technology_definition/tech.py 
TechnologyInfoclass for AnalogBase
Helper methods for electromigration 


BAG_technology_definition/tech.py 
Some technology parameters.

Open process definition for mosaic_BAG. To be used as a structural template. 
All information for any process removed.

## Supported component primitive types
For process configurationdevelopment,
four terminal nmos and pmos. Named as 'nmos4' and 'pmos4'
Four flavors hight,standard,low, and ultralow threshold voltage.
Device names formed as '<name>_<flavor>'. e,g 'nmos4_svt'
Ideal cap_ideal capacitance for pre-layout simulations
Ideal res_ideal resistance for pre-layout simulations.
cap_standard and res_standard to provide  'standard' capacitances 
and resistances. Implementaion mapping left for user.

## Developers guide
Copy this template to start building a process configuration and try to figure out the right numbers.
If it is impossible or diffficult it is an indication tha structure and documentaiton must be improved.

Suggestion: Start process configuration development from lvt transistors. 
Once they are in place and working, move to other flavors.

